<?php
namespace app\index\controller;
use app\common\controller\HomeBase;
use app\common\logic\Common as LogicCommon;



class Cidian extends  HomeBase
{
	
	/**
	 * 文章逻辑
	 */
	
	private static $commonLogic = null;

	
	public function _initialize()
	{
		parent::_initialize();
		
		self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class);
	}
   public function fengmian(){
 
   	$catelist=self::$commonLogic->getDataList('articlecate',['pid'=>0,'id'=>array('in','4,5,6,7,8,9,10,11,12,13')],true,'sort desc,create_time desc',false);
   	
   	$this->assign('catelist',$catelist);
   	return $this->fetch();
   	
   }
   public function index(){
 
   	
   	empty($this->param['cid']) ? $cid = 4 : $cid = $this->param['cid'];

   	$info=self::$commonLogic->getDataInfo('articlecate',['id'=>$cid]);
   	
   	$catelist=self::$commonLogic->getDataList('articlecate',['pid'=>0,'id'=>array('in','4,5,6,7,8,9,10,11,12,13')],true,'sort desc,create_time desc',false);
   	
   	
   	$list=self::$commonLogic->getDataList('article',['tid'=>$cid]);
   	
   	$this->assign('list',$list);
   	$this->assign('catelist',$catelist);
   	$this->assign('info',$info);
   	$this->assign('cid',$cid);
   	return $this->fetch();
   	
   }
   public function content($id){
   	$info=self::$commonLogic->getDataInfo('article',['id'=>$id]);
   	$this->assign('info',$info);
   	return $this->fetch();
   
   }

   
}
